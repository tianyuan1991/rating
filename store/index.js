import axios from 'axios'

const baselink = process.env.baseUrl

export const state = () => ({
  authUser: null,
  token: null
})

export const mutations = {
  SET_USER: function (state, user) {
    state.authUser = user
  },
  SET_TOKEN: function (state, token) {
    state.token = token
  }
}

export const actions = {
  nuxtServerInit({ commit }, { req }) {
    if (req.session && req.session.authUser) {
      commit('SET_USER', req.session.authUser)
      commit('SET_TOKEN', req.session.token)
    }
  },
  async login({ commit }, { email, password }) {
    try {
      const data = await axios.post(baselink + '/api/auth/', {
        email,
        password
      })
      commit('SET_USER', data.data.user)
      commit('SET_TOKEN', data.data.token)
      // commit('SET_TOKEN', data.data.token)

      // this.$cookies.set(
      //   'GCGR',
      //   { token: data.data.token },
      //   {
      //     path: '/',
      //     maxAge: 86400 * 6
      //   }
      // )

      return Promise.resolve(data.data)
    } catch (error) {
      if (error.response && error.response.status === 401) {
        throw new Error('Bad credentials')
      }
      throw error
    }
  },

  async verify_admin({ commit, state }, { token }) {
    try {
      const data = await axios.get(baselink + '/api/verify_admin/', {
        headers: {
          token: token
        }
      })

      return Promise.resolve(data.data)
    } catch (error) {
      if (error.response && error.response.status === 401) {
        throw new Error('Bad credentials')
      }
      throw error
    }
  },

  async logout({ commit }) {
    const data = await axios.get(baselink + '/api/logout/')
    commit('SET_USER', null)
    commit('SET_TOKEN', null)
    // this.$cookies.remove('GCGR')
    return Promise.resolve(data.data)
  }
}
