export const state = () => ({
  color: 'success',
  snack: '',
  afterReview: false
})

export const mutations = {
  setSnack(state, data) {
    state.color = data.color
    state.snack = data.snack
  },
  setReview(state, data) {
    state.afterReview = data.afterReview;
  }
}
