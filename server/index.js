const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

// ====== imported modules ======
const session = require('express-session')
const bodyParser = require('body-parser')
const cors = require("cors");

// ====== API ======
const routes = require('./routes/routes')
const ratingroutes = require('./routes/ratingroutes')
const reviewroutes = require('./routes/reviewroutes')

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // ==== Add modules ====
  app.use(bodyParser.json())

  app.use(
    session({
      secret: 'Harbin Institute of Technology',
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 24 * 60 * 60 * 1000 }
    })
  )

  // Mount routes to a group, e.g /api.
  app.use('/api', routes)
  app.use('/api/rating/', ratingroutes)
  app.use('/api/review/', reviewroutes)

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
