"use strict";

var express = require("express");
var ratingrouters = express.Router();
var ObjectId = require("mongodb").ObjectId;

const Rating = require("../model/rating");
const Review = require("../model/review");
const Tmp = require("../model/tmp");

// var md5 = require('js-md5')
var jwt = require("jsonwebtoken");

// Home api.
ratingrouters.get("/", (req, res) => {
  var output = {
    message: "This is rating routes"
  };
  res.json(output);
  return;
});

ratingrouters.post("/create", (req, res) => {
  var email = req.body.email;
  var rating = req.body.rating;
  var token = req.headers.token;

  jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
    if (err) {
      var output = {
        result: "error",
        message: "Token有误."
      };
      res.json(output);
      return;
    } else if (decoded.email !== email) {
      var output = {
        result: "error",
        message: "Token有误。"
      };
      res.json(output);
      return;
    } else {
      var newRating = Rating(rating);

      newRating.email = email;
      newRating.created_at = new Date();
      newRating.updated_at = new Date();

      newRating.save(function (err) {
        if (err) {
          var output = {
            result: "error",
            message: "添加新评阅失败，可能有缺失项，或者项目名已经被使用。"
          };
          res.json(output);
          return;
        } else {
          var output = {
            result: "success",
            message: "评阅 【" + rating.name + "】 添加成功!"
          };
          res.json(output);
          return;
        }
      });
    }
  });
});

// Fetch ratings belone to certain user
ratingrouters.get("/getmyratings", (req, res) => {
  var token = req.headers.token;
  jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
    if (err) {
      var output = {
        result: "error",
        message: "Token有误."
      };
      res.json(output);
      return;
    } else {
      let email = decoded.email;
      Rating.find({ email: email }, { _id: 1, name: 1, des: 1 }, function (
        err,
        ratings
      ) {
        if (err) throw err;
        else {
          var output = {
            result: "success",
            message: ratings.length + " 条用户信息已经获取。",
            ratings: ratings
          };
          res.json(output);
          return;
        }
      });
    }
  });
});

// Delete one rating
ratingrouters.post("/delete", (req, res) => {
  var id = req.body.id;
  var token = req.headers.token;

  jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
    if (err) {
      var output = {
        result: "error",
        message: "Token有误."
      };
      res.json(output);
      return;
    } else {
      Rating.findOneAndDelete(
        { _id: ObjectId(id), email: decoded.email },
        (err, pity) => {
          if (err) {
            var output = {
              result: "error",
              message: "项目似乎不存在。"
            };
            res.json(output);
            return;
          }

          Tmp.deleteMany({ id: pity._id }, function (err, _) {
            if (err) {
              var output = {
                result: "error",
                message: "暂存删除失败。"
              };
              res.json(output);
              return;
            }
            Review.deleteMany({ id: pity._id }, function (err, _) {
              if (err) {
                var output = {
                  result: "error",
                  message: "评阅删除失败。"
                };
                res.json(output);
                return;
              }
              var output = {
                result: "success",
                message: pity.name + "及过往评阅已经被彻底删除",
                name: pity.name
              };
              res.json(output);
              return;
            });
          });


        }
      );
    }
  });
});

// Fetch one rating
ratingrouters.post("/getonerating", (req, res) => {
  var id = req.body.id;
  var token = req.headers.token;

  jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
    if (err) {
      var output = {
        result: "error",
        message: "Token有误."
      };
      res.json(output);
      return;
    } else {
      Rating.findOne(
        { _id: ObjectId(id), email: decoded.email },
        (err, rating) => {
          if (err) {
            var output = {
              result: "error",
              message: "评阅不存在"
            };
            res.json(output);
            return;
          } else {
            var output = {
              result: "success",
              message: "评阅获取成功，你可以进行修改。",
              rating: rating
            };
            res.json(output);
            return;
          }
        }
      );
    }
  });
});

// Edit one rating
ratingrouters.post("/edit", (req, res) => {
  var email = req.body.email;
  var rating = req.body.rating;
  var deletereviews = req.body.deletereviews;
  rating.updated_at = new Date();

  var token = req.headers.token;
  jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
    if (err) {
      var output = {
        result: "error",
        message: "Token有错误。"
      };
      res.json(output);
      return;
    } else if (decoded.email !== email || decoded.email !== rating.email) {
      var output = {
        result: "error",
        message: "Token不匹配。"
      };
      res.json(output);
      return;
    } else {
      Rating.updateOne(
        { _id: ObjectId(rating._id), email: decoded.email },
        rating,
        (err, newrating) => {
          if (err) {
            var output = {
              result: "error",
              message: "编辑新评阅失败，可能您的新标题与某个已有标题重名了。"
            };
            res.json(output);
            return;
          } else {
            if (deletereviews) {
              Review.deleteMany({ rating_id: rating._id }, (err, result) => {
                if (err) throw err;
                else {
                  var output = {
                    result: "success",
                    message:
                      "编辑新评阅成功，并且删除了所有" +
                      result.n +
                      "条已有评阅。"
                  };
                  res.json(output);
                  return;
                }
              });
            } else {
              var output = {
                result: "success",
                message: "编辑新评阅成功，不用删除过往评阅。"
              };
              res.json(output);
              return;
            }
          }
        }
      );
    }
  });
});

// Fetch Open Ratings
ratingrouters.get("/getOpenRatings", (req, res) => {
  Rating.find(
    { $or: [{ queopen: true }, { resopen: true }] },
    {
      index: 1,
      title: 1,
      des: 1,
      queopen: 1,
      resopen: 1
    },
    function (err, ratings) {
      if (err) throw err;
      else {
        var output = {
          result: "success",
          message: ratings.length + " 个评阅项目已经获取。",
          ratings: ratings
        };
        res.json(output);
        return;
      }
    }
  );
});

// Fetch one rating for overview
ratingrouters.post("/getRatingForOverview", (req, res) => {
  var name = req.body.name;
  var code = req.body.code;

  Rating.findOne(
    // { name: name, $or: [{ queopen: true }, { resopen: true }] },
    { name: name, code: { $in: [code] } },
    { code: 0, resopen: 0 },
    (err, rating) => {
      if (err) throw err;
      if (rating === null) {
        var output = {
          result: "error",
          message: "评阅不存在"
        };
      } else {
        var output = {
          result: "success",
          message: "欢迎" + code + "，评阅" + name + "获取成功",
          rating: rating
        };
      }

      res.json(output);
      return;
    }
  );
});

// Fetch one rating for reviewing, there are three situations here:
// 1: It's the editor himself.
// 2: It's someone with queword.
// 3: It's an open and no password.

ratingrouters.post("/getRatingForReview", (req, res) => {
  var name = req.body.name;
  var code = req.body.code;
  var token = req.headers.token;

  if (token === "null") {
    Rating.findOne(
      {
        name: name,
        queopen: true,
        code: { $in: [code] }
      },
      { queword: 0, resword: 0, code: 0 },
      (err, rating) => {
        if (err) throw err;
        if (rating !== null) {
          var output = {
            result: "success",
            message: "评阅获取成功",
            rating: rating
          };
        } else {
          var output = {
            result: "error",
            message: "评阅不存在，或者结果尚未公开"
          };
        }
        res.json(output);
        return;
      }
    );
  } else {
    jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
      if (err) {
        var output = {
          result: "error",
          message: "Token有误."
        };
        res.json(output);
        return;
      } else {
        Rating.findOne(
          { name: name, email: decoded.email },
          { queword: 0, resword: 0, code: 0 },
          (err, rating) => {
            if (err) throw err;
            if (rating !== null) {
              var output = {
                result: "success",
                message: "您是项目所有人，评卷获取成功",
                rating: rating
              };
              res.json(output);
              return;
            } else {
              Rating.findOne(
                {
                  name: name,
                  queopen: true,
                  code: { $in: [code] }
                },
                { queword: 0, resword: 0, code: 0 },
                (err, rating) => {
                  if (err) throw err;
                  if (rating !== null) {
                    var output = {
                      result: "success",
                      message: "评阅获取成功",
                      rating: rating
                    };
                  } else {
                    var output = {
                      result: "error",
                      message: "评阅不存在，或者结果尚未公开"
                    };
                  }
                  res.json(output);
                  return;
                }
              );
            }
          }
        );
      }
    });
  }
});

ratingrouters.post("/getRatingForResult", (req, res) => {
  var name = req.body.name;
  var token = req.headers.token;

  if (token === "null") {
    Rating.findOne(
      {
        name: name,
        resopen: true
      },
      { code: 0 },
      (err, rating) => {
        if (err) throw err;
        if (rating !== null) {
          var output = {
            result: "success",
            message: "评阅获取成功",
            rating: rating
          };
        } else {
          var output = {
            result: "error",
            message: "评阅不存在，或者结果尚未公开"
          };
        }
        res.json(output);
        return;
      }
    );
  } else {
    jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
      if (err) {
        var output = {
          result: "error",
          message: "Token有误."
        };
        res.json(output);
        return;
      } else {
        Rating.findOne(
          { name: name, email: decoded.email },
          { code: 0 },
          (err, rating) => {
            if (err) throw err;
            if (rating !== null) {
              var output = {
                result: "success",
                message: "您是项目所有人，结果获取成功",
                rating: rating
              };
              res.json(output);
              return;
            } else {
              Rating.findOne(
                {
                  name: name,
                  resopen: true
                },
                { code: 0 },
                (err, rating) => {
                  if (err) throw err;
                  if (rating !== null) {
                    var output = {
                      result: "success",
                      message: "评阅获取成功",
                      rating: rating
                    };
                  } else {
                    var output = {
                      result: "error",
                      message: "评阅不存在，或者结果尚未公开"
                    };
                  }
                  res.json(output);
                  return;
                }
              );
            }
          }
        );
      }
    });
  }
});

module.exports = ratingrouters;
