"use strict";

var express = require("express");
var reviewroutes = express.Router();
var ObjectId = require("mongodb").ObjectId;

const Rating = require("../model/rating");
const Tmp = require("../model/tmp");
const Review = require("../model/review");

var jwt = require("jsonwebtoken");

reviewroutes.get("/", (req, res) => {
  var output = {
    message: "This is review routes"
  };
  res.json(output);
  return;
});

reviewroutes.post("/submittmp", (req, res) => {
  var id = req.body.id;
  var code = req.body.code;
  var result = req.body.result;

  Tmp.updateOne(
    { id: id, code: code },
    {
      result: result,
      updated_at: new Date()
    },
    (err, tmp) => {
      if (err) throw err;
      if (tmp.nModified === 1) {
        var output = {
          result: "success",
          message: "评阅 " + code + " 已经暂存更新!"
        };
        res.json(output);
        return;
      } else {
        Rating.findOne({ _id: ObjectId(id) }, { code: 1 }, (err, rating) => {
          if (err) {
            var output = {
              result: "error",
              message: "评阅不存在。"
            };
            res.json(output);
            return;
          }

          if (rating.code.includes(code)) {
            var newTmp = Tmp({
              id: id,
              code: code,
              result: result,
              updated_at: new Date()
            });

            newTmp.save(function (err) {
              if (err) {
                var output = {
                  result: "error",
                  message: "首次提交评阅暂存失败。"
                };
                res.json(output);
                return;
              } else {
                var output = {
                  result: "success",
                  message: "评阅 " + code + " 首次提交暂存成功!"
                };
                res.json(output);
                return;
              }
            });
          } else {
            var output = {
              result: "error",
              message: "您的评阅码错误。"
            };
            res.json(output);
            return;
          }
        });
      }
    }
  );
});

reviewroutes.post("/submitreview", (req, res) => {
  var id = req.body.id;
  var code = req.body.code;
  var submitReview = req.body.review;
  var comp = req.body.comp;
  var tmp_page = req.body.page;

  Review.findOne(
    {
      id: id,
      code: code
    },
    (err, review) => {
      if (err) throw err;
      if(review != null) {
        var review_arr = [];
        var page_arr = review.page;
        if(tmp_page != page_arr.length - 1){
          for(var i=0;i<5;i++){
            review.review[(tmp_page-1)*5+i] = submitReview[i];
          }
          page_arr[tmp_page] = true;
          review_arr = review.review;
        } else {
          page_arr = review.page.map(e => {
            return true;
          });
          review_arr = submitReview;
        }
        Review.updateOne(
          { id: id, code: code },
          {
            review: review_arr,
            complete: comp,
            page: page_arr,
            updated_at: new Date()
          },
          (err, review) => {
            if (err) throw err;
      
            if (review.nModified === 1) {
              var output = {
                result: "success",
                message: "评阅 " + code + " 已经更新提交!"
              };
              res.json(output);
              return;
            } 
          })
        } else { 
          Rating.findOne({ _id: ObjectId(id) }, { code: 1, options: 1 }, (err, rating) => {
          if (err) {
            var output = {
              result: "error",
              message: "评阅没有找到。"
            };
            res.json(output);
            return;
          }
          if (rating.code.includes(code)) {
            const page = parseInt(rating.options.length / 5) + 1;
            let empty_arr = new Array(rating.options.length).fill({option: '', answer: []})
            const arr_len = page + 1;
            let page_arr = new Array(arr_len).fill(false)
            if(page != tmp_page){
              for(var i=0;i<5;i++){
                empty_arr[(tmp_page-1)*5+i] = submitReview[i];
              }
              page_arr[tmp_page] = true;
            } else {
              for(var i=0;i<=page;i++){
                page_arr[i] = true
              }
              empty_arr = submitReview;
            }
            var newReview = Review({
              id: id,
              code: code,
              review: empty_arr,
              complete: comp,
              page: page_arr,
              created_at: new Date(),
              updated_at: new Date()
            });

            newReview.save(function (err) {
              if (err) {
                var output = {
                  result: "error",
                  message: "首次提交评阅失败。"
                };
                res.json(output);
                return;
              } else {
                var output = {
                  result: "success",
                  message: "评阅 " + code + " 首次提交成功!"
                };
                res.json(output);
                return;
              }
            });
          } else {
            var output = {
              result: "error",
              message: "您的评阅码错误。"
            };
            res.json(output);
            return;
          }
        });
      }
    })
});

reviewroutes.post("/getTmpReview", (req, res) => {
  var id = req.body.id;
  var code = req.body.code;

  Tmp.findOne(
    {
      id: id,
      code: code
    },
    (err, tmp) => {
      if (err) throw err;
      Review.findOne(
        {
          id: id,
          code: code
        },
        (err, review) => {
          if (err) throw err;
          // console.log(tmp.result, review)
          if (review === null && tmp === null) {
            var output = {
              result: "success",
              message: "欢迎" + code + "开始评阅！",
              tmp: null,
              review: null,
              comp: null,
              page: null,
            };
          } else if (review === null && tmp !== null) {
            var output = {
              result: "success",
              message: "tmp 返回",
              tmp: tmp.result,
              review: null,
              comp: null,
              page: null,
            };
          } else if (review !== null && tmp === null) {
            var output = {
              result: "success",
              message: "请继续评阅",
              tmp: review.review,
              review: review.review,
              comp: review.complete,
              page: review.page,
            };
          } else {
            var output = {
              result: "success",
              message: "请继续评阅",
              tmp: tmp.result,
              review: review.review,
              comp: review.complete,
              page: review.page,
            };
          }
          res.json(output);
          return;
        }
      );
    }
  );
});

// Fetch result of one rating, there are three situations here:
// 1: It's the editor himself.
// 2: It's someone with queword.
// 3: It's an open and no password.

reviewroutes.post("/getRatingResult", (req, res) => {
  var name = req.body.name;
  var token = req.headers.token;

  if (token === "null") {
    Rating.findOne(
      {
        name: name,
        resopen: true
      },
      (err, rating) => {
        if (err) throw err;
        if (rating !== null) {
          // Success here
          Review.find(
            {
              id: rating._id,
              complete: rating.options.length,
              code: { $in: rating.code }
            },
            { review: 1, complete: 1 },
            (err, reviews) => {
              if (err) throw err;
              rating.code = [];
              var output = {
                result: "success",
                message: "获取有效评阅结果" + reviews.length + "条！",
                rating: rating,
                reviews: reviews
              };
              res.json(output);
              return;
            }
          );
        } else {
          var output = {
            result: "error",
            message: "评阅不存在，或者结果尚未公开。"
          };
          res.json(output);
          return;
        }
      }
    );
  } else {
    jwt.verify(token, "Harbin Institute of Technology", function (err, decoded) {
      if (err) {
        var output = {
          result: "error",
          message: "Token有误."
        };
        res.json(output);
        return;
      } else {
        Rating.findOne({ name: name, email: decoded.email }, (err, rating) => {
          if (err) throw err;
          if (rating !== null) {
            // Success as editor here
            Review.find(
              {
                id: rating._id,
                complete: rating.options.length,
                code: { $in: rating.code }
              },
              { review: 1, complete: 1 },
              (err, reviews) => {
                if (err) throw err;
                rating.code = [];
                var output = {
                  result: "success",
                  message:
                    "您是项目创建人，获取有效评论结果" + reviews.length + "条！",
                  rating: rating,
                  reviews: reviews
                };
                res.json(output);
                return;
              }
            );
          } else {
            Rating.findOne(
              {
                name: name,
                resopen: true
              },
              (err, rating) => {
                if (err) throw err;
                if (rating !== null) {
                  // Success here
                  Review.find(
                    {
                      id: rating._id,
                      complete: rating.options.length,
                      code: { $in: rating.code }
                    },
                    { review: 1 },
                    (err, reviews) => {
                      if (err) throw err;
                      rating.code = [];
                      var output = {
                        result: "success",
                        message: "获取有效评论结果" + reviews.length + "条！",
                        rating: rating,
                        reviews: reviews
                      };
                      res.json(output);
                      return;
                    }
                  );
                } else {
                  var output = {
                    result: "error",
                    message: "评阅不存在，或者结果尚未公开"
                  };
                  res.json(output);
                  return;
                }
              }
            );
          }
        });
      }
    });
  }
});

module.exports = reviewroutes;
