'use strict'

var express = require('express')
var router = express.Router()
const User = require('../model/user')
var md5 = require('js-md5')
var jwt = require('jsonwebtoken')


// Home api.
router.get('/', (req, res) => {
  var output = {
    result: "success",
    message: "This is DFRating endpoint API entry."
  };
  res.json(output);
  return;
})

//rosemary conference APICS
// register api.
router.post('/register', (req, res) => {
  var signup_form = req.body
  console.log(signup_form)
  if (
    (typeof signup_form.email === 'undefined') |
    (typeof signup_form.password === 'undefined')
  ) {
    var output = {
      result: 'error',
      message: '有些必须字段尚未填写。'
    }
    res.json(output);
    return;
  }
  var newUser = User({
    email: signup_form.email,
    password: signup_form.password,
    admin: false,
    valid: false,
    random: Math.random()
      .toString(36)
      .substr(2),
    created_at: new Date()
  })
  console.log(newUser)
  newUser.save(function (err) {
    if (err) {
      var output = {
        result: 'error',
        message: '邮箱已经被注册.'
      }
    } else {
      // newUser.savefile()
      newUser.sendemail()
      var output = {
        result: 'success',
        message: '用户 ' + signup_form.email + ' 注册成功!'
      }
    }
    res.json(output);
    return;
  })
})

// User log in and get token.
router.post('/auth', (req, res) => {
  var email = req.body.email
  var password = md5(req.body.password)

  if ((typeof email === 'undefined') | (typeof password === 'undefined')) {
    var output = {
      result: 'error',
      message: '有些必须字段尚未填写。'
    }
    res.json(output);
    return;
  }

  User.findOne(
    { email: email, password: password },
    { password: 0 },
    function (err, user) {
      if (err) throw err
      else {
        if (user === null) {
          var output = {
            result: 'error',
            message: '信息有误。',
            user: user
          }
          res.json(output);
          return;
        } else {
          if (user.valid === false) {
            var output = {
              result: 'error',
              message: '邮箱尚未验证。',
              user: null
            }
            res.json(output);
            return;
          } else {
            const JWTToken = jwt.sign(
              {
                email: user.email,
                admin: user.admin
              },
              'Harbin Institute of Technology',{
                expiresIn: 60*60*24
              }
            )
            req.session.authUser = user
            req.session.token = JWTToken

            var output = {
              result: 'success',
              message: user.email + ' 已经登录。',
              user: user,
              token: JWTToken
              // token: JWTToken
            }
            res.json(output);
            return;
          }
        }
      }
    }
  )
})

router.get('/logout', (req, res) => {
  delete req.session.authUser
  var output = {
    result: 'success',
    message: '用户已经退出。'
  }
  res.json(output);
  return;
})

// ######################## Validate API ######################
router.post('/valid', (req, res) => {
  var email = req.body.email
  var random = req.body.random

  if ((typeof email === 'undefined') | (typeof random === 'undefined')) {
    var output = {
      result: 'error',
      message: '有些必须字段尚未填写'
    }
    res.json(output);
    return;
  }

  User.findOneAndUpdate(
    { email: email, random: random },
    { email: email, valid: true },
    { projection: { password: 0 } },
    function (err, user) {
      if (err) throw err
      else {
        if (user === null) {
          var output = {
            result: 'error',
            message: '该邮箱从未注册过。',
            user: user
          }
          res.json(output);
          return;
        } else {
          {
            var output = {
              result: 'success',
              message: '您的邮箱验证成功！',
              user: user
            }
            res.json(output);
            return;
          }
        }
      }
    }
  )
})

router.post('/resend', (req, res) => {
  var email = req.body.email

  if (typeof email === 'undefined') {
    var output = {
      result: 'error',
      message: '缺少邮箱地址。'
    }
    res.json(output);
    return;
  }

  User.findOne({ email: email }, function (err, user) {
    if (err) throw err
    else {
      if (user === null) {
        var output = {
          result: 'error',
          message: '邮箱从未注册过。',
          user: user
        }
        res.json(output);
        return;
      } else {
        {
          user.sendemail()
          var output = {
            result: 'success',
            message: '一封新的验证邮件已经发送'
          }
          res.json(output);
          return;
        }
      }
    }
  })
})

router.get('/verify_admin', (req, res) => {
  var token = req.headers.token
  jwt.verify(token, 'Harbin Institute of Technology', function (err, decoded) {
    if (err) {
      var output = {
        result: 'error',
        message: 'Token 有误.'
      }
    } else if (decoded.admin === false) {
      var output = {
        result: 'error',
        message: '您不是管理员。'
      }
    } else {
      var output = {
        result: 'success',
        message: '管理员权限验证成功！'
      }
    }
    res.json(output);
    return;
  })
})
// ######################## Admin API ######################
// Get all users.
router.get('/admin/get_all_user', (req, res) => {
  var token = req.headers.token
  jwt.verify(token, 'Harbin Institute of Technology', function (err, decoded) {
    if (err) {
      var output = {
        result: 'error',
        message: 'Token验证失败。'
      }
      res.json(output);
      return;
    } else if (decoded.admin === false) {
      var output = {
        result: 'error',
        message: '您不是管理员。'
      }
      res.json(output);
      return;
    } else {
      User.find({}, { password: 0 }, { sort: { created_at: -1 } }, function (
        err,
        users
      ) {
        if (err) throw err
        else {
          if (users === null) {
            var output = {
              result: 'error',
              message: '无法获取用户信息。',
              users: users
            }
            res.json(output);
            return;
          } else {
            var output = {
              result: 'success',
              message: users.length + ' 条用户信息已经获取。',
              users: users
            }
            res.json(output);
            return;
          }
        }
      })
    }
  })
})

// router.get('/users', (req, res, next) => {
//   res.body = users
//   next()
// })


module.exports = router;
