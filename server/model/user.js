var mongoose = require('mongoose')
var md5 = require('js-md5')
const jsonfile = require('jsonfile')
const file = './static/RegisterForm.json'
const nodemailer = require('nodemailer')

const config = require('config')

// const baselink = 'http://127.0.0.1:3000'
// const baselink = 'http://95.179.202.207'

// var MONGO_URI = 'mongodb://localhost:27017/DFRating?authSource=admin'

const baselink = config.get("BASE_LINK");
const MONGO_URI = config.get("MONGO_URI");


mongoose.connect(
  MONGO_URI,
  { useNewUrlParser: true }
)
mongoose.set('useCreateIndex', true)
mongoose.Promise = global.Promise

var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

var userSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  admin: Boolean,
  valid: Boolean,
  random: { type: String },
  created_at: Date
})

userSchema.methods.test = function () {
  this.name = this.name + '-test'
  return this.name
}

userSchema.methods.savefile = function () {
  jsonfile.writeFile(
    file,
    {
      name: this.name,
      email: this.email,
      password: this.password,
      organization: this.organization,
      research_direction: this.research_direction,
      admin: this.admin,
      valid: this.valid,
      random: this.random,
      created_at: this.created_at
    },
    { flag: 'a' },
    function (err) {
      if (err) console.error(err)
    }
  )
}

userSchema.methods.sendemail = function () {
  let transporter = nodemailer.createTransport({
    host: 'smtp.exmail.qq.com',
    secureConnection: true,
    use_authentication: true, // true for 465, false for other ports
    port: 465,
    auth: {
      user: 'tianyuan@cidatahub.com', // generated ethereal user
      pass: 'Abty2015' // generated ethereal password
    }
  })

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"树融评阅团队" <tianyuan@cidatahub.com>', // sender address
    to: this.email, // list of receivers
    subject: '树融评阅-邮箱验证 ✔', // Subject line
    // plain text body
    html:
      '<h4>感谢您注册树融评阅.</h4> <p>请点击下方邮件链接完成验证: <p> ' +
      baselink +
      '/valid?email=' +
      this.email +
      '&random=' +
      this.random +
      '<p>再次感谢！</p><p>树融评阅团队</p>' // html body
  }

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error)
    }
    console.log('Message sent: %s', info.messageId)
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))

    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  })
}

userSchema.pre('save', function (next) {
  var md5password = md5(this.password)
  this.password = md5password
  next()
})

var User = mongoose.model('user', userSchema)

module.exports = User
