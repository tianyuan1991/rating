var mongoose = require('mongoose')
const config = require('config');

const MONGO_URI = config.get("MONGO_URI");

// var MONGO_URI = 'mongodb://localhost:27017/DFRating?authSource=admin'

mongoose.connect(
  MONGO_URI,
  { useNewUrlParser: true }
)
mongoose.set('useCreateIndex', true)
mongoose.Promise = global.Promise

var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

var ratingSchema = new mongoose.Schema({
  email: { type: String, required: true },
  name: { type: String, required: true, unique: true },
  des: { type: String },
  method: { type: String },
  questions: { type: JSON, required: true },
  options: { type: JSON, required: true },
  hyperlink: { type: String },
  resopen: { type: Boolean, required: true },
  queopen: { type: Boolean, required: true },
  code: { type: JSON },
  created_at: Date,
  updated_at: Date
})

var Rating = mongoose.model('rating', ratingSchema)

module.exports = Rating
