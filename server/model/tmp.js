var mongoose = require('mongoose');
const config = require('config');

// var MONGO_URI = 'mongodb://localhost:27017/DFRating?authSource=admin'
// var MONGO_URI = 'mongodb://rate_user:user1@192.168.1.73:27017/Rating'
const MONGO_URI = config.get("MONGO_URI");

mongoose.connect(
  MONGO_URI,
  { useNewUrlParser: true }
)
mongoose.set('useCreateIndex', true)
mongoose.Promise = global.Promise

var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

var tmpSchema = new mongoose.Schema({
  id: { type: String, required: true },
  code: { type: String, required: true },
  result: { type: JSON, required: true },
  updated_at: Date
})

var Tmp = mongoose.model('tmp', tmpSchema)

module.exports = Tmp
