var mongoose = require('mongoose')
const config = require('config');

const MONGO_URI = config.get("MONGO_URI");
// var MONGO_URI = 'mongodb://localhost:27017/DFRating?authSource=admin'

mongoose.connect(
  MONGO_URI,
  { useNewUrlParser: true }
)
mongoose.set('useCreateIndex', true)
mongoose.Promise = global.Promise

var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

var reviewSchema = new mongoose.Schema({
  id: { type: String, required: true },
  code: { type: String, required: true },
  review: { type: JSON, required: true },
  complete: { type: Number, required: true },
  page: { type: JSON, required: true },
  created_at: Date,
  updated_at: Date
})

var Review = mongoose.model('review', reviewSchema)

module.exports = Review
